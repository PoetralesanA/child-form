﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupControl = New System.Windows.Forms.Panel()
        Me.btn_C = New System.Windows.Forms.Button()
        Me.btn_B = New System.Windows.Forms.Button()
        Me.btn_A = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.VIEWER = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupControl.SuspendLayout()
        Me.VIEWER.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupControl
        '
        Me.GroupControl.BackColor = System.Drawing.Color.FromArgb(CType(CType(30, Byte), Integer), CType(CType(30, Byte), Integer), CType(CType(32, Byte), Integer))
        Me.GroupControl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.GroupControl.Controls.Add(Me.btn_C)
        Me.GroupControl.Controls.Add(Me.btn_B)
        Me.GroupControl.Controls.Add(Me.btn_A)
        Me.GroupControl.Controls.Add(Me.Button1)
        Me.GroupControl.Dock = System.Windows.Forms.DockStyle.Left
        Me.GroupControl.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl.Name = "GroupControl"
        Me.GroupControl.Size = New System.Drawing.Size(153, 427)
        Me.GroupControl.TabIndex = 0
        '
        'btn_C
        '
        Me.btn_C.BackColor = System.Drawing.Color.Gray
        Me.btn_C.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btn_C.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_C.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_C.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btn_C.Location = New System.Drawing.Point(-2, 129)
        Me.btn_C.Name = "btn_C"
        Me.btn_C.Size = New System.Drawing.Size(153, 46)
        Me.btn_C.TabIndex = 3
        Me.btn_C.Text = "Data 3"
        Me.btn_C.UseVisualStyleBackColor = False
        '
        'btn_B
        '
        Me.btn_B.BackColor = System.Drawing.Color.Gray
        Me.btn_B.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btn_B.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_B.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_B.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btn_B.Location = New System.Drawing.Point(-2, 85)
        Me.btn_B.Name = "btn_B"
        Me.btn_B.Size = New System.Drawing.Size(153, 46)
        Me.btn_B.TabIndex = 2
        Me.btn_B.Text = "Data 2"
        Me.btn_B.UseVisualStyleBackColor = False
        '
        'btn_A
        '
        Me.btn_A.BackColor = System.Drawing.Color.Gray
        Me.btn_A.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btn_A.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_A.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_A.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btn_A.Location = New System.Drawing.Point(-2, 42)
        Me.btn_A.Name = "btn_A"
        Me.btn_A.Size = New System.Drawing.Size(153, 46)
        Me.btn_A.TabIndex = 1
        Me.btn_A.Text = "Data 1"
        Me.btn_A.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Gray
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button1.Location = New System.Drawing.Point(-2, -2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(153, 46)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Main"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'VIEWER
        '
        Me.VIEWER.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.VIEWER.Controls.Add(Me.Label1)
        Me.VIEWER.Dock = System.Windows.Forms.DockStyle.Fill
        Me.VIEWER.Location = New System.Drawing.Point(153, 0)
        Me.VIEWER.Name = "VIEWER"
        Me.VIEWER.Size = New System.Drawing.Size(925, 427)
        Me.VIEWER.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Font = New System.Drawing.Font("Segoe Print", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label1.Location = New System.Drawing.Point(-2, 144)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(920, 135)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "-=!!! PoetralesanA !!! =-"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FormMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1078, 427)
        Me.Controls.Add(Me.VIEWER)
        Me.Controls.Add(Me.GroupControl)
        Me.Name = "FormMain"
        Me.Text = "Main"
        Me.GroupControl.ResumeLayout(False)
        Me.VIEWER.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl As System.Windows.Forms.Panel
    Friend WithEvents btn_C As System.Windows.Forms.Button
    Friend WithEvents btn_B As System.Windows.Forms.Button
    Friend WithEvents btn_A As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents VIEWER As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class
