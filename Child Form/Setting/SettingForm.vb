﻿Public Class SettingForm
#Region "Setting Child Form"
    Private Shared Sub ClearChild(ByVal VIEWDATA As Panel) ' clear form child
        VIEWDATA.Controls.Clear()
    End Sub
    Public Shared Sub Child(ByVal FORM As Form,
                            ByVal VIEWDATA As Panel) ' add form child
        ClearChild(VIEWDATA)
        With FORM
            .TopLevel = False
            .TopMost = True
            .Dock = DockStyle.Fill
            VIEWDATA.Controls.Add(FORM)
            .Show()
        End With
    End Sub
#End Region
    Public Shared Sub Opacity(ByVal fm As Form) ' setting opacity/transparency form.
        Dim valueopacity As Double = 0.95
        fm.Opacity = valueopacity
    End Sub
    Public Shared Sub CenterPosision(ByVal form As Form) 'startup form posision
        form.StartPosition = FormStartPosition.CenterScreen
    End Sub
End Class